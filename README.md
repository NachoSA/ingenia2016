# README #

Este repositorio almacena el código fuente de los proyecto de la asignatura de Ingenia "Creación de Videojuegos y Simuladores".

### Flujo de Trabajo ###

* Cada coordinador debe hacer un fork del repositorio general.
* Cada miembro del equipo debe hacer un fork del repositorio de su coordinador.
* El coordinador debe revisar/aceptar los pull request de sus compañeros de equipo.
* Los coordinadores deben hacer un pull request del repositorio general cuando se les indique.

### Observaciones ###

* Atención: NO se debe subir a ninguno de los repositorios archivos resultado de compilación ni binarios que el IDE (Visual Studio) regenera. 
* TO BE CONTINUED